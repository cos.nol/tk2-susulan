from django.contrib import admin
from django.urls import include, path
from . import views
from .views import *

app_name = 'login_app'

urlpatterns = [
    path('', views.home_views, name='home'),
]
