from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .urls import *
from .views import *

class TestLoginApp(TestCase):
    def test_home_views(self): 
        found = resolve('/')         
        self.assertEqual(found.func, home_views)

    def test_home_url(self):
        response = Client().get('/')
        self.assertEquals(200, response.status_code)

    def test_login_is_success_and_session_end_after_30_days(self):
        kiriman = {'username':'someone', 'password' : 'tutifruti123', 'ingatsaya': 'on'}
        response = Client().post('/', kiriman, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(response.status_code, 200)

    def test_login_is_success_and_session_end_when_browser_closed(self):
        kiriman = {'username':'someone', 'password' : 'tutifruti123', 'ingatsaya': 'on'}
        response = Client().post('/', kiriman, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(response.status_code, 200)
    # def test_login_is_success_and_session_end_when_browser_closed(self):
    # def test_login_is_not_success(self):
