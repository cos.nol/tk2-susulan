from django.shortcuts import render
# from django.template import RequestContext
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse

def home_views(request):
    # print(request.POST.get("logout") == "terklik")

    if request.is_ajax:
        if request.method == "POST": 
            username = request.POST.get("username")
            # print(username)
            password = request.POST.get("password")
            # print(password)
            ingatsaya = request.POST.get("ingatsaya")
            # print(ingatsaya)
            user = authenticate(request, username=username, password=password)

            if user is not None and ingatsaya is not None:
                login(request, user)
                request.session.set_expiry(2592000) # 2592000 = 30 days = 30*24*60*60 seconds
                return HttpResponseRedirect('/')

            elif user is not None and ingatsaya is None:
                login(request, user)
                request.session.set_expiry(0)
                return HttpResponseRedirect('/')

            elif user is None:
                respon_json = { "feedback":"Username ATAU Password salah" }
                return JsonResponse({'data' : respon_json})

        elif request.POST.get("logout") == "Logout":
            logout(request)
            return HttpResponseRedirect('/')
            
    # if request.is_ajax and request.POST.get("logout") == "terklik":
    #     logout(request)
    #     print("FUNGSI LOGOUT")
    #     return HttpResponseRedirect('/')

    return render(request, 'home.html')
